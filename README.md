# GameWer Source

Source code of GameWer

# Information
EN:
This is not the official & exact source of GameWer. 
It's a somewhat accurate reversed binary that I spent time on.
The only thing I have removed from this is the hash checks.

RU:
Это не официальный и точный источник GameWer.
Это довольно точный обратный двоичный файл, над которым я потратил много времени.
Единственное, что я удалил из этого, - это хеш-проверки.